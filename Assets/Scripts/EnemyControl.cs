﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyControl : MonoBehaviour
{

    public GameObject graphics;
    public GameObject explosionenemy;
    public float speed = 5f;
    private Vector3 iniPos;
   

    private BoxCollider2D myCollider;

    public static MyGameManager instance;
    public static MyGameManager getInstance()
    {
        return instance;
    }

    void Start()
    {
        iniPos = transform.position;
    }

    void Awake()
    {
        myCollider = GetComponent<BoxCollider2D>();
    }

    void Update()
    {
        transform.Translate(Vector3.down * speed * Time.deltaTime);
    }

  
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Bullet" || other.tag == "Player")
        {
            Explode();
        }
        else if (other.tag == "Finish")
        {
            Reset();
        }

       
    }
    private void Explode()
    {
        
        GameObject explosion = (GameObject)Instantiate(explosionenemy);
        explosion.transform.position = transform.position;
        graphics.SetActive(false);
        myCollider.enabled = false;
        

        Destroy(gameObject);
        
    }

    protected void Reset()
    {
        transform.position = iniPos;
        
    }
}