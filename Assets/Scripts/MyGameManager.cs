﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MyGameManager : MonoBehaviour {
	public Text scoreText;
	public Text livesCounter;
    public int lives = 3;
    private int score = 0;
    
    public GameObject pauseMenu;

	public static MyGameManager instance;

	void Awake(){
		if (instance == null) {
			instance = this;
		}
	}

	public static MyGameManager getInstance(){
		return instance;
	}

	// Use this for initialization
	void Start () {
		lives = 3;
		score = 0;
        Updatescore();
        scoreText.text = score.ToString("D5");
    }

    void Updatescore()
    {
        scoreText.text = "Score: " + score;

            }
	
    public void AddScore(int value)
    {
        score += value;
        scoreText.text = score.ToString("D5");
        Updatescore();
      
    }



	public void LoseLive(){
		lives--;
        livesCounter.text = "Lives: " + lives.ToString ();
	}

	  

    public void Pause()
    {
        Time.timeScale = 0;
        pauseMenu.SetActive(true);
    }
    

    public void Resume()
    {
        Time.timeScale = 1;
        pauseMenu.SetActive(false);
    }

    public void Menu()
    {
        SceneManager.LoadScene("MainMenu");
    }



    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Pause();
        }

        if (lives == 0)
        {
            Debug.Log("Game Over");
            SceneManager.LoadScene("gameOver");
        }
    }
    
}
