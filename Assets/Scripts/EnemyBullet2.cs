﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet2 : MonoBehaviour
{

    float speed;
    Vector2 direction;
    bool isRdy;

    
    void Awake()
    {
        speed = 5f;
        isRdy = false;
    }

    public void SetDirection(Vector2 direction)
    {
        direction = direction.normalized;
        isRdy = true;        
    }

    void Start()
    {

    }

    void Update()
    {
        if (isRdy)
        {
            Vector2 position = transform.position;
            position += direction * speed * Time.deltaTime;

            transform.position = position;
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Finish" || other.tag == "Player")
        {
            Destroy(gameObject);
        }
    }
}
