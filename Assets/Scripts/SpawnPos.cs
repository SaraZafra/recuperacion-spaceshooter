﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPos : MonoBehaviour
{

    public Vector2 axis;

    private Vector3 positionAct;

    void Update()
    {
        transform.Translate(axis * Time.deltaTime);
        positionAct = transform.position;


        if (positionAct.x > 9)
        {
            axis.x -= Random.Range(2, 9);
        }
        if (positionAct.x < -7)
        {
            axis.x -= Random.Range(-2, -9);
        }

        transform.position = positionAct;
    }
}