﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{

    public GameObject Enemy;
    public GameObject Enemy2;

    float maxSpawnRateInSeconds = 3f;

    private void Start()
    {
        Invoke("SpawnEnemy", maxSpawnRateInSeconds);
        

        InvokeRepeating("IncreaseSpawnRate", 0f, 30f);
    }

    private void Update()
    {
        
    }

    void SpawnEnemy()
    {
        Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));

        Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));

        GameObject anEnemy = (GameObject)Instantiate(Enemy);
        anEnemy.transform.position = new Vector2(Random.Range(min.x, max.x), max.y);

        GameObject anEnemy2 = (GameObject)Instantiate(Enemy2);
        anEnemy2.transform.position = new Vector2(Random.Range(min.x, max.x), max.y);

        ScheduleNextEnemySpawn();
    }

    void ScheduleNextEnemySpawn()
    {
        float spawnInNSeconds;
        if(maxSpawnRateInSeconds > 2f)
        {
            spawnInNSeconds = Random.Range(2f, maxSpawnRateInSeconds);
        }
        else
        {
            spawnInNSeconds = 3f;

            Invoke("SpawnEnemy", spawnInNSeconds);
        }
    }

    void IncreaseSpawnRate()
    {
        if (maxSpawnRateInSeconds > 1f)
            maxSpawnRateInSeconds--;

        if (maxSpawnRateInSeconds == 1f)
            CancelInvoke("IncreaseSpawnRate");
    }
}