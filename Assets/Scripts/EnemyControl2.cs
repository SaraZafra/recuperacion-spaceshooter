﻿using UnityEngine;
using System.Collections;

public class EnemyControl2 : MonoBehaviour
{
    private Transform player;
    public float speed;   

    public GameObject graphics;
    public GameObject explosionenemy;
    

    public static MyGameManager instance;
    private BoxCollider2D myCollider;

    public static MyGameManager getInstance()
    {
        return instance;
    }

    void Awake()
    {
        myCollider = GetComponent<BoxCollider2D>();

    }

    protected void Update()
    {
        
        transform.Translate(Vector3.down * speed * Time.deltaTime);
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Bullet" || other.tag == "Player")
        {
            Explode();
        }
        else if (other.tag == "Finish")
        {
            Reset();
        }
    }
    private void Explode()
    {
        
        GameObject explosion = (GameObject)Instantiate(explosionenemy);
        explosion.transform.position = transform.position;
        graphics.SetActive(false);
        myCollider.enabled = false;

        Destroy(gameObject);
        
    }

    protected void Reset()
    {
        myCollider.enabled = true;
        graphics.SetActive(true);
        Destroy(gameObject);
    }
}
